#!/bin/bash

###############################################################
#
#  AUTEUR:   Xavier
#
#  DESCRIPTION: list ip address with ifconfig
###############################################################

ifconfig | awk '/inet / {print $2}'
