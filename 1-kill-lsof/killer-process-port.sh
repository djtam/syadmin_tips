#!/bin/bash

###############################################################
#  TITRE: 
#
#  AUTEUR:   Xavki
#
#  DESCRIPTION: killer les process en écoute sur le port 80
#  ATTENTION utilisation du sudo ne pas faire nimp
###############################################################

#code
sudo lsof -i :80 | awk 'NR>1 {print $2}' | xargs sudo kill
