#!/bin/bash

###############################################################
#  AUTEUR:   Xavier
#
#  DESCRIPTION: test internal and externe command
###############################################################


a=1
b=2

echo ""
echo ""
echo "TEST : commande interne let"
type let
time for ((i=1;i<=10000;i++)); do
	let c=a+b+i
done
echo $c


echo ""
echo ""
echo "TEST : commande externe expr"
type expr
time for ((i=1;i<=10000;i++)); do
	c=$(expr $a + $b + $i)
done
echo $c
